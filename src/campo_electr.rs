pub const K: u128 = 8987551787;

pub const ep0: f64 =  0.000000000008854187817;

pub struct CampoElectroMagneticoEscalar<T> {
    pub E: T,
    pub E2: T,
    pub F: T,
    pub q: T,
    pub r: T,
    pub V: T,
    pub Ep: T,
    pub Ec: T,
    pub W: T,
    pub u: T,
    pub d: T,
    pub d2: T,
    pub q2: T,
}
pub struct CampoElectroMagneticoVectorial<T> {
    pub E: Vec<T>,
    pub F: Vec<T>,
    pub q: f32,
    pub r: Vec<T>,
    pub V: f32,
    pub Ep: Vec<T>,
    pub Ec: Vec<T>,
    pub W: Vec<T>,
    pub d: Vec<T>,
    pub u: Vec<T>
}

impl CampoElectroMagneticoEscalar<f32> {
    pub fn new() -> CampoElectroMagneticoEscalar<f32> {
        CampoElectroMagneticoEscalar {
            E: 0f32,
            E2: 0f32,
            F: 0f32,
            q: 0f32,
            r: 0f32,
            V: 0f32,
            Ep: 0f32,
            Ec: 0f32,
            W: 0f32,
            d: 0f32,
            u: 0f32,
            d2: 0f32,
            q2: 0f32
        }
    }
}

impl CampoElectroMagneticoEscalar<f64> {
    pub fn new() -> CampoElectroMagneticoEscalar<f64> {
        CampoElectroMagneticoEscalar {
            E: 0f64,
            E2: 0f64,
            F: 0f64,
            q: 0f64,
            r: 0f64,
            V: 0f64,
            Ep: 0f64,
            Ec: 0f64,
            W: 0f64,
            d: 0f64,
            u: 0f64,
            d2: 0f64,
            q2: 0f64
        }
    }
}

impl CampoElectroMagneticoEscalar<i128> {
    pub fn new() -> CampoElectroMagneticoEscalar<i128> {
        CampoElectroMagneticoEscalar {
            E: 0i128,
            E2: 0i128,
            F: 0i128,
            q: 0i128,
            r: 0i128,
            V: 0i128,
            Ep: 0i128,
            Ec: 0i128,
            W: 0i128,
            d: 0i128,
            u: 0i128,
            d2: 0i128,
            q2: 0i128,
        }
    }
}

pub trait CampoElectroMagneticoEscalari128{
     
     fn Intencidad_campo_electrico_F_qi128(&mut self,F: i128, q: i128) -> i128;

     fn Intencidad_campo_electrico_F_q_IVi128(&mut self) -> i128;

     fn Carga_E_Fi128(&mut self, F: i128, E: i128) -> i128;

     fn Carg_E_F_IVi128(&mut self) -> i128;

     fn disti128(&mut self, d: i128);

     fn Inten_Camp_disti128(&mut self, q: i128, d: i128) -> i128;

     fn Inten_Camp_dist_2i128(&mut self, q: i128, d: i128) -> i128;
     
}

impl CampoElectroMagneticoEscalari128 for CampoElectroMagneticoEscalar<i128> {

    fn Intencidad_campo_electrico_F_qi128(&mut self,F: i128, q: i128) -> i128 {
        
        self.q = q;
        self.F = F;
        self.E = F/q;
        self.E
    }

     fn Intencidad_campo_electrico_F_q_IVi128(&mut self) -> i128 {
        self.E = self.F/self.q;
        self.E
     }

     fn Carga_E_Fi128(&mut self, F: i128, E: i128) -> i128 {
        self.F = F;
        self.E = E;
        self.q = F/E;
        self.q
     }

     fn Carg_E_F_IVi128(&mut self) -> i128 {
        self.q = self.F/self.E;
        self.E
     }

     fn disti128(&mut self, d: i128) {
        self.d = d;
     }

     fn Inten_Camp_disti128(&mut self, q: i128, d: i128) -> i128 {
        self.q = q;
        self.d = d;
        self.E = ((9i128 * 10i128.pow(9)) * self.q )/ self.d.pow(2) ;
        self.E
    }

     fn Inten_Camp_dist_2i128(&mut self, q: i128, d: i128) -> i128 {
        self.q2 = q;
        self.d2 = d;
        self.E2 = (K as i128) * ( self.q2 / self.d2.pow(2) );
        self.E2
     }

}
pub trait CampoElectroMagneticoEscalarf64 {

    fn Intencidad_campo_electrico_F_qf64(&mut self,F: f64, q: f64) -> f64;

    fn Intencidad_campo_electrico_F_q_IVf64(&mut self) -> f64;

    fn Carga_E_Ff64(&mut self, F: f64, E: f64) -> f64;

    fn Carg_E_F_IVf64(&mut self) -> f64;

    fn distf64(&mut self, d: f64);

    fn Inten_Camp_distf64(&mut self, q: f64, d: f64) -> f64;

    fn Inten_Camp_dist_2f64(&mut self, q: f64, d: f64) -> f64;

}

impl CampoElectroMagneticoEscalarf64 for CampoElectroMagneticoEscalar<f64> {
    fn Intencidad_campo_electrico_F_qf64(&mut self,F: f64, q: f64) -> f64 {
       self.q = q;
       self.F = F;
       self.E = F/q;
       self.E
   }
    fn Intencidad_campo_electrico_F_q_IVf64(&mut self) -> f64 {
       self.E = self.F/self.q;
       self.E
   }
    fn Carga_E_Ff64(&mut self, F: f64, E: f64) -> f64 {
       self.F = F;
       self.E = E;
       self.q = F/E;
       self.q
   }
    fn Carg_E_F_IVf64(&mut self) -> f64 {
       self.q = self.F/self.E;
       self.E
   }
    fn distf64(&mut self, d: f64){
       self.d = d;
   }
    fn Inten_Camp_distf64(&mut self, q: f64, d: f64) -> f64 {
       self.q = q;
       self.d = d;
       self.E = ((9f64 * 10f64.powf(9f64)) * self.q )/ self.d.powf(2f64) ;
       self.E
   }
    fn Inten_Camp_dist_2f64(&mut self, q: f64, d: f64) -> f64 {
       self.q2 = q;
       self.d2 = d;
       self.E2 = (K as f64) * ( self.q2 / self.d2.powf(2f64) );
       self.E2
   }
}

pub trait CampoElectroMagneticoEscalarf32 {

     fn Intencidad_campo_electrico_F_qf32(&mut self,F: f32, q: f32) -> f32;

     fn Intencidad_campo_electrico_F_q_IVf32(&mut self) -> f32;

     fn Carga_E_Ff32(&mut self, F: f32, E: f32) -> f32;

     fn Carg_E_F_IVf32(&mut self) -> f32;

     fn distf32(&mut self, d: f32);

     fn Inten_Camp_distf32(&mut self, q: f32, d: f32) -> f32;

     fn Inten_Camp_dist_2f32(&mut self, q: f32, d: f32) -> f32;

}

impl CampoElectroMagneticoEscalarf32 for CampoElectroMagneticoEscalar<f32> {
     fn Intencidad_campo_electrico_F_qf32(&mut self,F: f32, q: f32) -> f32 {
        self.q = q;
        self.F = F;
        self.E = F/q;
        self.E
    }
     fn Intencidad_campo_electrico_F_q_IVf32(&mut self) -> f32 {
        self.E = self.F/self.q;
        self.E
    }
     fn Carga_E_Ff32(&mut self, F: f32, E: f32) -> f32 {
        self.F = F;
        self.E = E;
        self.q = F/E;
        self.q
    }
     fn Carg_E_F_IVf32(&mut self) -> f32 {
        self.q = self.F/self.E;
        self.E
    }
     fn distf32(&mut self, d: f32){
        self.d = d;
    }
     fn Inten_Camp_distf32(&mut self, q: f32, d: f32) -> f32 {
        self.q = q;
        self.d = d;
        self.E = ((9f32 * 10f32.powf(9f32)) * self.q )/ self.d.powf(2f32) ;
        self.E
    }
     fn Inten_Camp_dist_2f32(&mut self, q: f32, d: f32) -> f32 {
        self.q2 = q;
        self.d2 = d;
        self.E2 = (K as f32) * ( self.q2 / self.d2.powf(2f32) );
        self.E2
    }
}