mod operaciones;
mod campo_electr;
use campo_electr::CampoElectroMagneticoEscalar;
use campo_electr::CampoElectroMagneticoEscalarf64;
use operaciones::BasOpF32;

fn main() {
    problema_1();
    problema_2();

}

fn problema_1() {
    println!("Problema 1. Determina la intensidad de campo
    eléctrico en el punto medio entre una carga de - 60
    micro C y otra carga + 40 micro C. Las Cargas están
    separadas 70 mm en el aire");

    
    let mut campo_1: CampoElectroMagneticoEscalar<f64> = CampoElectroMagneticoEscalar::<f64>::new();

        let dist: f64 = ((35f32).dist_from_mm_f32()) as f64;

        let q1: f64 = ((-60f32).pr_si_micro_f32()) as f64;
        
        let q2: f64 = ((40f32).pr_si_micro_f32()) as f64;
        campo_1.Inten_Camp_distf64(q1,dist);
        campo_1.Inten_Camp_dist_2f64(q2, dist);
        let rest1 = campo_1.E + campo_1.E2;
        
        println!("
             carga 1      carga 2
              -              +
              ᳂ -----x------ ᳂
                 atraccion al -
                <--- energia resultante es: {} N/C", rest1 );  
}
fn problema_2(){
    println!("\n\nProblema 2. Una carga de 8 nano C está a 80 mm a la
    derecha de una carga de + 4 nano C. ¿Cuál es la
    intensidad del campo eléctrico resultante en un punto
    localizado a 24 mm de la carga de la izquierda?, entre
    las dos cargas");
    

    
    let mut campo_2: CampoElectroMagneticoEscalar<f64> = CampoElectroMagneticoEscalar::<f64>::new();

        let dist1: f64 = ((24f32).dist_from_mm_f32()) as f64;
        let dist2: f64 = ((24f32+ 80f32).dist_from_mm_f32()) as f64;

        let q1: f64 = ((4f32).pr_si_nano_f32()) as f64;
        let q2: f64 = ((8f32).pr_si_nano_f32()) as f64;

        campo_2.Inten_Camp_distf64(q1,dist1);
        campo_2.Inten_Camp_dist_2f64(q2, dist2);
        let rest2 = campo_2.E + campo_2.E2;
        println!("
              4 nano C     8 nano C
donde medire  carga 1      carga 2
               +             +
        x ---- ᳂ ----------- ᳂
          24mm       80mm        
         |__________________|
                r2
         |____|
           r1  
        <-----energia resultante es: {} N/C", rest2 );
}
