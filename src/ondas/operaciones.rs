use std::*; 
pub trait BasOpF32 {
    fn diez_powf32(&self, algo: f32) -> f32;
    fn dist_from_cm_f32(&self) -> f32;
    fn dist_from_mm_f32(&self) -> f32;
    fn pr_si_yotta_f32(&self) -> f32 ;
    fn pr_si_zetta_f32(&self) -> f32 ;
    fn pr_si_exa_f32(&self) -> f32   ;
    fn pr_si_peta_f32(&self) -> f32  ;
    fn pr_si_tera_f32(&self) -> f32  ;
    fn pr_si_giga_f32(&self) -> f32  ;
    fn pr_si_mega_f32(&self) -> f32  ;
    fn pr_si_kilo_f32(&self) -> f32  ;
    fn pr_si_hecto_f32(&self) -> f32 ;
    fn pr_si_deca_f32(&self) -> f32  ;
    fn pr_si_deci_f32(&self) -> f32  ;
    fn pr_si_centi_f32(&self) -> f32 ;
    fn pr_si_mili_f32(&self) -> f32  ;
    fn pr_si_micro_f32(&self) -> f32 ;
    fn pr_si_nano_f32(&self) -> f32  ;
    fn pr_si_pico_f32(&self) -> f32  ;
    fn pr_si_femto_f32(&self) -> f32 ;
    fn pr_si_atto_f32(&self) -> f32  ;
    fn pr_si_zepto_f32(&self) -> f32 ;
    fn pr_si_yocto_f32(&self) -> f32 ;
}
impl BasOpF32 for f32 {

     fn diez_powf32(&self, algo: f32) -> f32 {
         self  * (10 as f32).powf(algo)
    }
    //  fn 10_powf32((&self, algo: f32) -> f32 {
    // self * 10f32.powf(algo)
    //}

     fn dist_from_cm_f32(&self) -> f32 {
        self / 100f32
    }

     fn dist_from_mm_f32(&self) -> f32 {
        self / 1000f32
    }

     fn pr_si_yotta_f32(&self) -> f32 {
        self.diez_powf32( 24f32)
    }

     fn pr_si_zetta_f32(&self) -> f32 {
        self.diez_powf32(21f32)
    }

     fn pr_si_exa_f32(&self) -> f32 {
        self.diez_powf32(18f32)
    }

     fn pr_si_peta_f32(&self) -> f32 {
        self.diez_powf32(15f32)
    }

     fn pr_si_tera_f32(&self) -> f32 {
       self.diez_powf32(12f32)
    }

     fn pr_si_giga_f32(&self) -> f32 {
       self.diez_powf32(9f32)
    }

     fn pr_si_mega_f32(&self) -> f32 {
        self.diez_powf32(6f32)
    }

     fn pr_si_kilo_f32(&self) -> f32 {
        self.diez_powf32(3f32)
    }

     fn pr_si_hecto_f32(&self) -> f32 {
        self.diez_powf32(2f32)
    }

     fn pr_si_deca_f32(&self) -> f32 {
        self * 10f32
    }

     fn pr_si_deci_f32(&self) -> f32 {
        self.diez_powf32(-1f32)
    }

     fn pr_si_centi_f32(&self) -> f32 {
        self.diez_powf32(-2f32)
    }

     fn pr_si_mili_f32(&self) -> f32 {
        self.diez_powf32(-3f32)
    }

     fn pr_si_micro_f32(&self) -> f32 {
        self.diez_powf32(-6f32)
    }

     fn pr_si_nano_f32(&self) -> f32 {
        self.diez_powf32(-9f32)
    }

     fn pr_si_pico_f32(&self) -> f32 {
        self.diez_powf32(-12f32)
    }

     fn pr_si_femto_f32(&self) -> f32 {
        self.diez_powf32(-15f32)
    }

     fn pr_si_atto_f32(&self) -> f32 {
        self.diez_powf32(-18f32)
    }

     fn pr_si_zepto_f32(&self) -> f32 {
        self.diez_powf32(-21f32)
    }

     fn pr_si_yocto_f32(&self) -> f32 {
        self.diez_powf32(-24f32)
    }
}

