use std::*;
mod campo_electr;

pub struct Onda {
    vel: f32, 
    lamb: f32, 
    frec: f32 , 
    c_frec: f32,
    per: f32 ,
    em_vel: f32,
}

impl Onda {
    pub fn new() -> Onda {
        Onda {
            vel : 0f32, lamb : 0f32, frec: 0f32 ,
            per: 0f32, em_vel: 0f32, c_frec: 0f32,
         }
    }
}

